<?php

namespace App\Controllers;
use App\Models\Grupomodel;

class Home extends BaseController
{
    public function index()
    {
        return view('welcome_message');
    }
     public function grupos(){
        $data['title'] = 'Listado de Alumnos';
        $textoabuscar = $this->request->getPost('texto');
        $grupos = new Grupomodel();
        $data['grupos'] = $grupos ->like(['codigo'=>strtoupper($textoabuscar)]) ->findAll();
        /*echo "<pre>";
        print_r($data);
        echo "</pre>";*/
        
        return view('grupo/lista',$data);
     }

         public function formgrupos(){
        $data['title'] = 'Listado de Alumnos';
        return view('grupo/form',$data);
    }

 }
