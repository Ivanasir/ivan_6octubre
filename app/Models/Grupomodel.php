<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Grupomodel
 *
 * @author a024463185v
 */

namespace App\Models;
use CodeIgniter\Model;


class Grupomodel extends Model {
    
    protected $table = 'grupos';
    protected $primarykey = 'id';
    protected $returnType = 'object';        
}
